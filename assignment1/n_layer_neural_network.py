__author__ = 'tan_nguyen'
import numpy as np
from sklearn.datasets import load_breast_cancer
from sklearn import datasets, linear_model, preprocessing
import matplotlib.pyplot as plt

def generate_data():
    '''
    generate data
    :return: X: input data, y: given labels
    '''
    np.random.seed(0)
    X, y = datasets.make_moons(200, noise=0.20)
    return X, y

def generate_cancer():
    '''
    generate data
    :return: X: input data, y: given labels
    '''
    X, y = load_breast_cancer(return_X_y = True)
    print(np.max(X), np.min(X))
    X_scaled = preprocessing.scale(X)
    return X_scaled, y


def plot_decision_boundary(pred_func, X, y):
    '''
    plot the decision boundary
    :param pred_func: function used to predict the label
    :param X: input data
    :param y: given labels
    :return:
    '''
    # Set min and max values and give it some padding
    x_min, x_max = X[:, 0].min() - .5, X[:, 0].max() + .5
    y_min, y_max = X[:, 1].min() - .5, X[:, 1].max() + .5
    h = 0.01
    # Generate a grid of points with distance h between them
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))
    # Predict the function value for the whole gid
    Z = pred_func(np.c_[xx.ravel(), yy.ravel()])
    Z = Z.reshape(xx.shape)
    # Plot the contour and training examples
    plt.contourf(xx, yy, Z, cmap=plt.cm.Spectral)
    plt.scatter(X[:, 0], X[:, 1], c=y, cmap=plt.cm.Spectral)
    plt.show()

########################################################################################################################
########################################################################################################################
# YOUR ASSSIGMENT STARTS HERE
# FOLLOW THE INSTRUCTION BELOW TO BUILD AND TRAIN A 3-LAYER NEURAL NETWORK
########################################################################################################################
########################################################################################################################
class DeepNeuralNetwork(object):
    """
    This class builds and trains a neural network
    """
    def __init__(self, nn_input_dim, nn_hidden_dim , nn_output_dim, num_layers, actFun_type='tanh', reg_lambda=0.01, seed=0):
        '''
        :param nn_input_dim: input dimension
        :param nn_hidden_dim: the number of hidden units
        :param nn_output_dim: output dimension
        :param actFun_type: type of activation function. 3 options: 'tanh', 'sigmoid', 'relu'
        :param reg_lambda: regularization coefficient
        :param seed: random seed
        '''
        self.nn_input_dim = nn_input_dim
        self.nn_hidden_dim = nn_hidden_dim
        self.nn_output_dim = nn_output_dim
        self.actFun_type = actFun_type
        self.reg_lambda = reg_lambda
        self.num_layers = num_layers - 1
        self.layers = []
        
        # initialize the weights and biases in the network
        self.layers.append(Layer(nn_input_dim, nn_hidden_dim, lambda x: self.actFun(x, type=self.actFun_type), lambda x: self.diff_actFun(x, type=self.actFun_type), reg_lambda, seed))
        for i in range(num_layers - 2):
            self.layers.append(Layer(nn_hidden_dim, nn_hidden_dim, lambda x: self.actFun(x, type=self.actFun_type), lambda x: self.diff_actFun(x, type=self.actFun_type), reg_lambda, seed))

        self.layers.append(Layer(nn_hidden_dim, nn_output_dim, lambda x: x, lambda x: np.ones(np.shape(x)), reg_lambda, seed))

    def actFun(self, z, type):
        '''
        actFun computes the activation functions
        :param z: net input
        :param type: Tanh, Sigmoid, or ReLU
        :return: activations
        '''

        # YOU IMPLMENT YOUR actFun HERE
        if type == 'ReLU':
            return np.maximum(np.zeros(np.shape(z)),z)
        elif type == 'Sigmoid':
            return 1 / (1 + np.exp(-z))
        elif type == 'tanh':
            return np.tanh(z)

        raise Exception("Invlaid activation function type ")

    def diff_actFun(self, z, type):
        '''
        diff_actFun computes the derivatives of the activation functions wrt the net input
        :param z: net input
        :param type: Tanh, Sigmoid, or ReLU
        :return: the derivatives of the activation functions wrt the net input
        '''

        # YOU IMPLEMENT YOUR diff_actFun HERE
        if type == 'ReLU':
            #return 0 if np.max(0,z)==0 else 1
            return np.sign(np.maximum(np.zeros(np.shape(z)),z))
        elif type == 'Sigmoid':
            temp = self.actFun(z, type)
            return temp * (1 - temp)
        elif type == "tanh":
            return 1 / np.square(np.cosh(z))

        raise Exception("Invalid activation derivative type")

    def feedforward(self, X):
        '''
        feedforward builds a 3-layer neural network and computes the two probabilities,
        one for class 0 and one for class 1
        :param X: input data
        :param actFun: activation function
        :return:
        '''

        # YOU IMPLEMENT YOUR feedforward HERE
        an = np.copy(X)
        for layer in self.layers:
            an = layer.feedforward(an)

        exp_scores = np.exp(an)
        self.probs = exp_scores / np.sum(exp_scores, axis=1, keepdims=True)
        return None

    def calculate_loss(self, X, y):
        '''
        calculate_loss computes the loss for prediction
        :param X: input data
        :param y: given labels
        :return: the loss for prediction
        '''
        num_examples = len(X)
        self.feedforward(X)
        # Calculating the loss

        # YOU IMPLEMENT YOUR CALCULATION OF THE LOSS HERE
        y_hot = np.zeros(np.shape(self.probs))
        for i in range(len(y)):
            y_hot[i,y[i]] = 1.0

        data_loss = -1 * np.sum(np.multiply(y_hot, np.log(self.probs)))

        # Add regulatization term to loss (optional)
        data_loss += self.reg_lambda / 2 * sum([ np.sum( np.square( layer.W  )  ) for layer in self.layers])
        return (1. / num_examples) * data_loss

    def predict(self, X):
        '''
        predict infers the label of a given data point X
        :param X: input data
        :return: label inferred
        '''
        self.feedforward(X)
        return np.argmax(self.probs, axis=1)


    def fit_model(self, X, y, epsilon=0.01, num_passes=20000, print_loss=True):
        '''
        fit_model uses backpropagation to train the network
        :param X: input data
        :param y: given labels
        :param num_passes: the number of times that the algorithm runs through the whole dataset
        :param print_loss: print the loss or not
        :return:
        '''
        # Gradient descent.
        for i in range(0, num_passes):
            # Forward propagation
            self.feedforward(X)

            # Backpropagation
            num_examples = len(X)
            delta_n = self.probs
            delta_n[range(num_examples), y] -= 1

            for j in range(len(self.layers) -1, -1, -1):
                delta_n, dW, db = self.layers[j].backprop(delta_n, num_examples)
                dW += self.reg_lambda * self.layers[j].W
                self.layers[j].W += -epsilon * dW
                self.layers[j].b += -epsilon * db

            # Optionally print the loss.
            # This is expensive because it uses the whole dataset, so we don't want to do it too often.
            if print_loss and i % 1000 == 0:
                print("Loss after iteration %i: %f" % (i, self.calculate_loss(X, y)))

    def visualize_decision_boundary(self, X, y):
        '''
        visualize_decision_boundary plots the decision boundary created by the trained network
        :param X: input data
        :param y: given labels
        :return:
        '''
        plot_decision_boundary(lambda x: self.predict(x), X, y)

    def accuracy(self, X, y):
        pred = self.predict(X)
        print(pred)
        return np.average(pred==y)

class Layer():
    def __init__(self, input_dim, output_dim, actFun, diff_actFun, reg_lambda=0.01, seed=0):
        self.input_dim = input_dim
        self.output_dim = output_dim
        self.actFun = actFun
        self.diff_actFun = diff_actFun
        self.reg_lambda = reg_lambda

#        np.random.seed(seed)
        self.W = np.random.randn(self.input_dim, self.output_dim) / np.sqrt(self.input_dim)
        self.b = np.zeros((1, self.output_dim))

    def feedforward(self, X): 
        self.input = X
        self.z = np.matmul(X, self.W) + self.b
        self.a = self.actFun(self.z)
        return self.a

    def backprop(self, mod_delta_next, num_examples):
        delta_n = np.multiply(mod_delta_next, self.diff_actFun(self.z))
        dW = np.zeros(np.shape(self.W))
        db = np.zeros(np.shape(self.b))

        for i in range(num_examples):
            dW += np.outer(self.input[i,:], delta_n[i,:])
            db += delta_n[i,:]
        mod_delta_n = np.dot(delta_n, np.transpose(self.W)) 
        return mod_delta_n, dW, db

def main():
    # # generate and visualize Make-Moons dataset
    #X, y = generate_data()
    X, y = generate_cancer()
    #plt.scatter(X[:, 0], X[:, 1], s=40, c=y, cmap=plt.cm.Spectral)
    #plt.show()

    model = DeepNeuralNetwork(nn_input_dim=np.shape(X)[1], nn_hidden_dim=5, nn_output_dim=2, num_layers=5, actFun_type='tanh', reg_lambda=0.0001, seed=10)
    #print(model.calculate_loss(X,y))
    model.fit_model(X,y, epsilon=5e-4, num_passes=5000)
    print(model.accuracy(X,y))
    #model.fit_model(X,y, epsilon=5e-4, num_passes=5000)
    #model.visualize_decision_boundary(X,y)

if __name__ == "__main__":
    main()
