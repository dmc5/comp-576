__author__ = 'tan_nguyen'

import os
import time

# Load MNIST dataset
from tensorflow.examples.tutorials.mnist import input_data
mnist = input_data.read_data_sets('MNIST_data', one_hot=True)

# Import Tensorflow and start a session
import tensorflow as tf
sess = tf.InteractiveSession()

def leaky_relu(x, alpha=0.2):
    return tf.maximum(x, alpha * x)

def weight_variable(shape):
    '''
    Initialize weights
    :param shape: shape of weights, e.g. [w, h ,Cin, Cout] where
    w: width of the filters
    h: height of the filters
    Cin: the number of the channels of the filters
    Cout: the number of filters
    :return: a tensor variable for weights with initial values
    '''
    # IMPLEMENT YOUR WEIGHT_VARIABLE HERE
    initial = tf.truncated_normal(shape, stddev=0.1)
    W = tf.Variable(initial)
    return W

def bias_variable(shape):
    '''
    Initialize biases
    :param shape: shape of biases, e.g. [Cout] where
    Cout: the number of filters
    :return: a tensor variable for biases with initial values
    '''

    # IMPLEMENT YOUR BIAS_VARIABLE HERE
    initial = tf.constant(0.1, shape=shape)
    b = tf.Variable(initial)
    return b

def conv2d(x, W):
    '''
    Perform 2-D convolution
    :param x: input tensor of size [N, W, H, Cin] where
    N: the number of images
    W: width of images
    H: height of images
    Cin: the number of channels of images
    :param W: weight tensor [w, h, Cin, Cout]
    w: width of the filters
    h: height of the filters
    Cin: the number of the channels of the filters = the number of channels of images
    Cout: the number of filters
    :return: a tensor of features extracted by the filters, a.k.a. the results after convolution
    '''

    # IMPLEMENT YOUR CONV2D HERE
    h_conv = tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='SAME')
    return h_conv

def max_pool_2x2(x):
    '''
    Perform non-overlapping 2-D maxpooling on 2x2 regions in the input data
    :param x: input data
    :return: the results of maxpooling (max-marginalized + downsampling)
    '''

    # IMPLEMENT YOUR MAX_POOL_2X2 HERE
    h_max = tf.nn.max_pool(x, ksize=[1, 2, 2, 1],strides=[1, 2, 2, 1], padding='SAME') 
    return h_max

def variable_summaries(var, tag):
    '''
    Attach a lot of summaries to a Tensor (for TensorBoard visualization).
    Taken from the Tensorboard tutorial
    '''
    with tf.name_scope('summaries'):
        mean = tf.reduce_mean(var)
        tf.summary.scalar(tag + ' mean', mean)
        with tf.name_scope('stddev'):
            stddev = tf.sqrt(tf.reduce_mean(tf.square(var - mean)))
        tf.summary.scalar(tag + ' stddev', stddev)
        tf.summary.scalar(tag + ' max', tf.reduce_max(var))
        tf.summary.scalar(tag + ' min', tf.reduce_min(var))
        tf.summary.histogram(tag + ' histogram', var)

def main():
    # Specify training parameters
    result_dir = './results_alt/' # directory where the results from the training are saved
    max_step = 5500 # the maximum iterations. After max_step iterations, the training will stop no matter what

    start_time = time.time() # start timing

    # FILL IN THE CODE BELOW TO BUILD YOUR NETWORK

    # placeholders for input data and input labeles
    x = tf.placeholder(tf.float32, shape=[None, 784])
    y_ = tf.placeholder(tf.float32, shape=[None, 10])

    # reshape the input image
    x_image = tf.reshape(x, [-1, 28, 28, 1])

    # first convolutional layer
    W_conv1 = weight_variable([5, 5, 1, 32])
    b_conv1 = bias_variable([32])
    h_conv1 = leaky_relu(conv2d(x_image, W_conv1) + b_conv1)
    h_pool1 = max_pool_2x2(h_conv1)

    # second convolutional layer
    W_conv2 = weight_variable([5, 5, 32, 64])
    b_conv2 = bias_variable([64])
    h_conv2 = leaky_relu(conv2d(h_pool1, W_conv2) + b_conv2)
    h_pool2 = max_pool_2x2(h_conv2)

    # densely connected layer
    W_fc1 = weight_variable([7 * 7 * 64, 1024])
    b_fc1 = bias_variable([1024])
    h_pool2_flat = tf.reshape(h_pool2, [-1, 7*7*64])
    h_fc1 = leaky_relu(tf.matmul(h_pool2_flat, W_fc1) + b_fc1)

    # dropout
    keep_prob = tf.placeholder(tf.float32)
    h_fc1_drop = tf.nn.dropout(h_fc1, keep_prob)

    # softmax
    W_fc2 = weight_variable([1024, 10])
    b_fc2 = bias_variable([10])
    y_conv = tf.matmul(h_fc1_drop, W_fc2) + b_fc2


    # FILL IN THE FOLLOWING CODE TO SET UP THE TRAINING

    # setup training
    cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=y_, logits=y_conv))
    train_step = tf.train.MomentumOptimizer(1e-4,0.8).minimize(cross_entropy)
    correct_prediction = tf.equal(tf.argmax(y_conv, 1), tf.argmax(y_, 1))
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

    # Add a scalar summary for the snapshot loss.
    #tf.summary.scalar(cross_entropy.op.name, cross_entropy)
    tf.summary.scalar("Cross Entropy Loss", cross_entropy)

    vars_to_watch = [W_conv1, b_conv1,h_conv1, h_pool1,
                     W_conv2, b_conv2,h_conv2, h_pool2,
                     W_fc1, b_fc1, h_fc1, W_fc2, b_fc2,]
    tags_to_watch = ["Weights Conv 1", "Biases Conv 1", "ReLU activations 1", "Maxpool activations 1",
                     "Weights Conv 2", "Biases Conv 2", "ReLU activations 2", "Maxpool activations 2",
                     "Weights FC 1", "Biases FC1", "Activations FC1", "Weights FC 2", "Biases FC2"]
    for i in range(len(vars_to_watch)):
        variable_summaries(vars_to_watch[i], tags_to_watch[i])

    # Build the summary operation based on the TF collection of Summaries.
    summary_op = tf.summary.merge_all()

    # Add the variable initializer Op.
    init = tf.initialize_all_variables()

    # Create a saver for writing training checkpoints.
    saver = tf.train.Saver()

    # Instantiate a SummaryWriter to output summaries and the Graph.
    summary_writer = tf.summary.FileWriter(result_dir + "training/", sess.graph)

    test_writer = tf.summary.FileWriter(result_dir + "test/", sess.graph)

    # Run the Op to initialize the variables.
    sess.run(init)

    # Initialize varibles for test and validation errors
    batch_size = 500
    test_size = len(mnist.test.labels)
    val_size = len(mnist.validation.labels)

    # run the training
    for i in range(max_step):
        batch = mnist.train.next_batch(50) # make the data batch, which is used in the training iteration.
                                            # the batch size is 50
        if i%100 == 0:
            # output the training accuracy every 100 iterations
            train_accuracy = accuracy.eval(feed_dict={
                x:batch[0], y_:batch[1], keep_prob: 1.0})
            print("step %d, training accuracy %g"%(i, train_accuracy))

            # Update the events file which is used to monitor the training (in this case,
            # only the training loss is monitored)
            summary_str = sess.run(summary_op, feed_dict={x: batch[0], y_: batch[1], keep_prob: 0.5})
            summary_writer.add_summary(summary_str, i)
            summary_writer.flush()

        # save the checkpoints every 1100 iterations
        if i % 1100 == 0 or i == max_step:
            checkpoint_file = os.path.join(result_dir, 'checkpoint')
            saver.save(sess, checkpoint_file, global_step=i)
            
            # Calculates the test and validation error
            test_accs = []
            valid_accs = []
            for j in range(int(test_size / batch_size)):
                batch_test_images = mnist.test.images[j*batch_size:(j+1)*batch_size,:]
                batch_test_labels = mnist.test.labels[j*batch_size:(j+1)*batch_size,:]
                test_accs.append(sess.run([accuracy],feed_dict={x: batch_test_images, y_: batch_test_labels, keep_prob: 1.0}))
            test_acc = tf.reduce_mean(test_accs)
            test_sum = tf.Summary(value=[tf.Summary.Value(tag="Test Error", simple_value = 1 - test_acc.eval())])
            test_writer.add_summary(test_sum, i)

            for j in range(int(val_size / batch_size)):
                batch_valid_images = mnist.validation.images[j*batch_size:(j+1)*batch_size,:]
                batch_valid_labels = mnist.validation.labels[j*batch_size:(j+1)*batch_size,:]
                valid_accs.append(sess.run([accuracy], feed_dict={x: batch_valid_images, y_: batch_valid_labels, keep_prob: 1.0}))
            valid_acc = tf.reduce_mean(valid_accs)
            valid_sum = tf.Summary(value=[tf.Summary.Value(tag="Validation Error", simple_value = 1 - valid_acc.eval())])
            test_writer.add_summary(valid_sum, i)
            test_writer.flush()
         
        train_step.run(feed_dict={x: batch[0], y_: batch[1], keep_prob: 0.5}) # run one train_step

    # print test error
    acc = 0
    for i in range(int(test_size / batch_size)):
        batch_images = mnist.test.images[i*batch_size:(i+1)*batch_size,:]
        batch_labels = mnist.test.labels[i*batch_size:(i+1)*batch_size,:]
        acc += (float(batch_size) / test_size) * accuracy.eval(feed_dict={x: batch_images, y_: batch_labels, keep_prob: 1.0})

    print("test accuracy %g"%acc)

    stop_time = time.time()
    print('The training takes %f second to finish'%(stop_time - start_time))

if __name__ == "__main__":
    main()
