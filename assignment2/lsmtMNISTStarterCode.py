import tensorflow as tf 
#from tensorflow.python.ops import rnn, rnn_cell
import numpy as np 
import time

from tensorflow.examples.tutorials.mnist import input_data

start_time = time.time() 

mnist = input_data.read_data_sets('MNIST_data', one_hot=True)
 
learningRate = 5e-4
trainingIters =300000
batchSize = 100
displayStep = 100

nInput = 28 #we want the input to take the 28 pixels
nSteps = 28 #every 28
nHidden = 256 #number of neurons for the RNN
nClasses = 10 #this is MNIST so you know

x = tf.placeholder('float', [None, nSteps, nInput])
y = tf.placeholder('float', [None, nClasses])

weights = {
	'out': tf.Variable(tf.random_normal([nHidden, nClasses]))
}

biases = {
	'out': tf.Variable(tf.random_normal([nClasses]))
}

def RNN(x, weights, biases):
	x = tf.transpose(x, [1,0,2])
	x = tf.reshape(x, [-1, nInput])
	#x = tf.split(0, nSteps, x) #configuring so you can get it as needed for the 28 pixels
	x = tf.split(x, nSteps, 0) #configuring so you can get it as needed for the 28 pixels

	#cell = tf.contrib.rnn.BasicLSTMCell(nHidden) #find which lstm to use in the documentation
	#cell = tf.contrib.rnn.BasicRNNCell(nHidden)
	cell = tf.contrib.rnn.GRUCell(nHidden)
	

	outputs, states = tf.contrib.rnn.static_rnn(cell, x, dtype=tf.float32) #for the rnn where to get the output and hidden state 

	return tf.matmul(outputs[-1], weights['out'])+ biases['out']

rnn_out = RNN(x, weights, biases)
pred = tf.nn.softmax(rnn_out)


#optimization
#create the cost, optimization, evaluation, and accuracy
#for the cost softmax_cross_entropy_with_logits seems really good

cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=y, logits=rnn_out))
optimizer = tf.train.AdamOptimizer(learningRate).minimize(cost)

correctPred = tf.equal(tf.argmax(rnn_out, 1), tf.argmax(y, 1))
accuracy = tf.reduce_mean(tf.cast(correctPred, tf.float32))


tf.summary.scalar("Cross Entropy Loss", cost)
tf.summary.scalar("Train Accuracy", accuracy)

result_dir = './rnn/' 
summary_op = tf.summary.merge_all()

init = tf.initialize_all_variables()

with tf.Session() as sess:
	sess.run(init)
	step = 1
	summary_writer = tf.summary.FileWriter(result_dir + "train/", sess.graph)
	test_writer = tf.summary.FileWriter(result_dir + "test/", sess.graph)

	testData = mnist.test.images.reshape((-1, nSteps, nInput))
	testLabel = mnist.test.labels

	while step * batchSize < trainingIters:
		batchX, batchY = mnist.train.next_batch(batchSize) #mnist has a way to get the next batch
		batchX = batchX.reshape((batchSize, nSteps, nInput))

		sess.run(optimizer, feed_dict={x:batchX, y:batchY})

		if step % displayStep == 0:
			acc = accuracy.eval(feed_dict={x:batchX, y:batchY})
			loss = cost.eval(feed_dict={x:batchX, y:batchY})
			summary_str = sess.run(summary_op, feed_dict={x:batchX, y:batchY})
			summary_writer.add_summary(summary_str, step)
			summary_writer.flush()

			test_val = sess.run(accuracy, feed_dict={x:testData, y:testLabel})
			test_sum = tf.Summary(value=[tf.Summary.Value(tag="Test Accuracy", simple_value=test_val)])
			test_writer.add_summary(test_sum, step)
			test_writer.flush()

			print("Iter " + str(step*batchSize) + ", Minibatch Loss= " + \
                  "{:.6f}".format(loss) + ", Training Accuracy= " + \
                  "{:.5f}".format(acc))

		step +=1
	print('Optimization finished')

	print("Testing Accuracy:", \
        sess.run(accuracy, feed_dict={x:testData, y:testLabel}))


	stop_time = time.time()
	print('The training takes %f second to finish'%(stop_time - start_time))
