from scipy import misc
import numpy as np
import tensorflow as tf
import random
import matplotlib.pyplot as plt
import matplotlib as mp
import os

# --------------------------------------------------
# setup

def weight_variable(shape):
    '''
    Initialize weights
    :param shape: shape of weights, e.g. [w, h ,Cin, Cout] where
    w: width of the filters
    h: height of the filters
    Cin: the number of the channels of the filters
    Cout: the number of filters
    :return: a tensor variable for weights with initial values
    '''

    # IMPLEMENT YOUR WEIGHT_VARIABLE HERE
    initial = tf.truncated_normal(shape, stddev=0.1)
    W = tf.Variable(initial)
    return W

def bias_variable(shape):
    '''
    Initialize biases
    :param shape: shape of biases, e.g. [Cout] where
    Cout: the number of filters
    :return: a tensor variable for biases with initial values
    '''

    # IMPLEMENT YOUR BIAS_VARIABLE HERE
    initial = tf.constant(0.1, shape=shape)
    b = tf.Variable(initial)
    return b

def conv2d(x, W):
    '''
    Perform 2-D convolution
    :param x: input tensor of size [N, W, H, Cin] where
    N: the number of images
    W: width of images
    H: height of images
    Cin: the number of channels of images
    :param W: weight tensor [w, h, Cin, Cout]
    w: width of the filters
    h: height of the filters
    Cin: the number of the channels of the filters = the number of channels of images
    Cout: the number of filters
    :return: a tensor of features extracted by the filters, a.k.a. the results after convolution
    '''

    # IMPLEMENT YOUR CONV2D HERE
    h_conv = tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='SAME')
    return h_conv

def max_pool_2x2(x):
    '''
    Perform non-overlapping 2-D maxpooling on 2x2 regions in the input data
    :param x: input data
    :return: the results of maxpooling (max-marginalized + downsampling)
    '''

    # IMPLEMENT YOUR MAX_POOL_2X2 HERE
    h_max = tf.nn.max_pool(x, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME')
    return h_max

def variable_summaries(var, tag):
    '''
    Attach a lot of summaries to a Tensor (for TensorBoard visualization).
    Taken from the Tensorboard tutorial
    '''
    with tf.name_scope('summaries'):
        mean = tf.reduce_mean(var)
        tf.summary.scalar(tag + ' mean', mean)
        with tf.name_scope('stddev'):
            stddev = tf.sqrt(tf.reduce_mean(tf.square(var - mean)))
        tf.summary.scalar(tag + ' stddev', stddev)
        tf.summary.scalar(tag + ' max', tf.reduce_max(var))
        tf.summary.scalar(tag + ' min', tf.reduce_min(var))
        tf.summary.histogram(tag + ' histogram', var)


ntrain =  1000 # per class
ntest =  100 # per class
nclass =  10 # number of classes
imsize = 28
nchannels = 1
batchsize = 200

Train = np.zeros((ntrain*nclass,imsize,imsize,nchannels))
Test = np.zeros((ntest*nclass,imsize,imsize,nchannels))
LTrain = np.zeros((ntrain*nclass,nclass))
LTest = np.zeros((ntest*nclass,nclass))

itrain = -1
itest = -1
for iclass in range(0, nclass):
    for isample in range(0, ntrain):
        path = os.getcwd() + '/CIFAR10/Train/%d/Image%05d.png' % (iclass,isample)
        im = misc.imread(path); # 28 by 28
        im = im.astype(float)/255
        itrain += 1
        Train[itrain,:,:,0] = im
        LTrain[itrain,iclass] = 1 # 1-hot lable
    for isample in range(0, ntest):
        path = os.getcwd() + '/CIFAR10/Test/%d/Image%05d.png' % (iclass,isample)
        im = misc.imread(path); # 28 by 28
        im = im.astype(float)/255
        itest += 1
        Test[itest,:,:,0] = im
        LTest[itest,iclass] = 1 # 1-hot lable

sess = tf.InteractiveSession()

tf_data = tf.placeholder(tf.float32, shape=[None, imsize, imsize, nchannels]) #tf variable for the data, remember shape is [None, width, height, numberOfChannels] 
tf_labels = tf.placeholder(tf.float32, shape=[None, nclass]) #tf variable for labels

# --------------------------------------------------
# model
#create your model
W_conv1 = weight_variable([5, 5, 1, 32])
b_conv1 = bias_variable([32])
h_conv1 = tf.nn.relu(conv2d(tf_data, W_conv1) + b_conv1)
h_pool1 = max_pool_2x2(h_conv1)

# second convolutional layer
W_conv2 = weight_variable([5, 5, 32, 64])
b_conv2 = bias_variable([64])
h_conv2 = tf.nn.relu(conv2d(h_pool1, W_conv2) + b_conv2)
h_pool2 = max_pool_2x2(h_conv2)

# densely connected layer
W_fc1 = weight_variable([7 * 7 * 64, 1024])
b_fc1 = bias_variable([1024])
h_pool2_flat = tf.reshape(h_pool2, [-1, 7*7*64])
h_fc1 = tf.nn.relu(tf.matmul(h_pool2_flat, W_fc1) + b_fc1)

# dropout
keep_prob = tf.placeholder(tf.float32)
h_fc1_drop = tf.nn.dropout(h_fc1, keep_prob)

# softmax
W_fc2 = weight_variable([1024, 10])
b_fc2 = bias_variable([10])
y_conv = tf.matmul(h_fc1_drop, W_fc2) + b_fc2

# --------------------------------------------------
# loss
#set up the loss, optimization, evaluation, and accuracy
cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=tf_labels, logits=y_conv))
#optimizer = tf.train.AdamOptimizer(1e-3).minimize(cross_entropy)
optimizer = tf.train.RMSPropOptimizer(5e-4).minimize(cross_entropy)
#optimizer = tf.train.GradientDescentOptimizer(5e-3).minimize(cross_entropy)
correct_prediction = tf.equal(tf.argmax(y_conv, 1), tf.argmax(tf_labels, 1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))


tf.summary.scalar("Cross Entropy Loss", cross_entropy)
tf.summary.scalar("Train Accuracy", accuracy)
variable_summaries(h_conv1, "Activations in First Conv")
variable_summaries(h_conv2, "Activations in Second Conv")

sess.run(tf.initialize_all_variables())
# --------------------------------------------------
# Visualization
#
#with tf.variable_scope('visualization'):
x_min = tf.reduce_min(W_conv1)
x_max = tf.reduce_max(W_conv1)
W_conv1_scale = (W_conv1 - x_min) / (x_max - x_min)
W_conv1_tran = tf.transpose(W_conv1_scale, [3,0,1,2])
num = 0
col = []
for j in range(4):
    row = []
    for j in range(8):
        row.append(W_conv1_tran[num,0:5,0:5,:])
        num += 1
        row.append(tf.ones([5,1,1]))
    col.append(tf.concat(row, axis=1))
    col.append(tf.ones([1,48,1]))
conv_im = tf.reshape(tf.concat(col, axis=0),[1,24,48,1])


filter_sum = tf.summary.image('Weights', conv_im, max_outputs=1)
filter_sum_two = tf.summary.image('Weights', W_conv1_tran, max_outputs=2)

result_dir = './convnet/' 
summary_op = tf.summary.merge_all()
summary_writer = tf.summary.FileWriter(result_dir + "train/", sess.graph)

test_writer = tf.summary.FileWriter(result_dir + "test/", sess.graph)
# --------------------------------------------------
# optimization

batch_xs = np.zeros((batchsize, imsize, imsize, nchannels)) #setup as [batchsize, width, height, numberOfChannels] and use np.zeros()
batch_ys = np.zeros((batchsize, nclass)) #setup as [batchsize, the how many classes] 
for i in range(10000): # try a small iteration size once it works then continue
    #perm = np.arange(nsamples)
    perm = np.arange(ntrain*nclass)
    np.random.shuffle(perm)
    for j in range(batchsize):
        batch_xs[j,:,:,:] = Train[perm[j],:,:,:]
        batch_ys[j,:] = LTrain[perm[j],:]

    if i%100 == 0:
        #calculate train accuracy and print it
        train_acc = accuracy.eval(feed_dict={tf_data:batch_xs, tf_labels:batch_ys, keep_prob:1.0})
        print("step %d, training accuracy %g"%(i, train_acc))

        summary_str = sess.run(summary_op, feed_dict={tf_data:batch_xs, tf_labels:batch_ys, keep_prob: 1.0})
        summary_writer.add_summary(summary_str, i)
        summary_writer.flush()

        #Computes the test accuracy
        test_accs = []
        test_size = nclass*ntest
        for k in range(int(test_size / batchsize)):
            for j in range(batchsize):
                batch_xs[j,:,:,:] = Test[j+(k*batchsize),:,:,:]
                batch_ys[j,:] = LTest[j+(k*batchsize),:]
            test_accs.append(sess.run([accuracy], feed_dict={tf_data: batch_xs, tf_labels: batch_ys, keep_prob: 1.0}))
        test_acc = tf.reduce_mean(test_accs)
        test_sum = tf.Summary(value=[tf.Summary.Value(tag="Test Accuracy", simple_value=test_acc.eval())])
        test_writer.add_summary(test_sum, i)
        test_writer.flush()



    optimizer.run(feed_dict={tf_data:batch_xs, tf_labels:batch_ys, keep_prob:0.4}) # dropout only during training

# --------------------------------------------------
# test

# print test error
test_size = nclass*ntest
acc = 0
for i in range(int(test_size / batchsize)):
    for j in range(batchsize):
        batch_xs[j,:,:,:] = Test[j+(i*batchsize),:,:,:]
        batch_ys[j,:] = LTest[j+(i*batchsize),:]
    acc += (float(batchsize) / test_size) * accuracy.eval(feed_dict={tf_data: batch_xs, tf_labels: batch_ys, keep_prob: 1.0})

print("test accuracy %g"%acc)

sess.close()
